#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pyedflib
import numpy as np
from edf.edfio import EDFIO

# http://pyedflib.readthedocs.io/en/latest/
# https://github.com/holgern/pyedflib/blob/master/pyedflib/edfwriter.py

class EDF(EDFIO):

    def __init__(self, param={}):
        # применяем конструктор родителя
        super(EDF, self).__init__()

        self.result = None

        if not param or type(param) != dict:
            return

        fname_debug = param.get('debug')
        if fname_debug:
            res = self.open_file(fname_debug, 'a', 'debug')
            if res != 0:
                print('cant open debug file', str(res))

        return


    # деструктор
    def __del__(self):
        ftypes = []
        for ftype in self.files:
            ftypes.append(ftype)
        for ftype in ftypes:
            self.close_file(ftype)
        return

    # 
    def read_signal(self, channel):
        if type(channel) != int or channel < 0:
            return self.set_error(109, "Invalid parameter 'channel', must be int")

        edf = self.files.get('in', {}).get('edf')
        params = self.files.get('in', {}).get('params')
        if not edf or not params:
            return self.set_error(110, 'EDF / BDF file is not open, cant read signal')

        sig_count = params.get('signal_count', 0)
        if sig_count < 1:
            return self.set_error(111, 'There is no signals in EDF / BDF file')

        if channel >= sig_count:
            return self.set_error(112, "'channel'={c} is greater then signals count in file".format(c=channel))

        self.result = list(edf.readSignal(channel))
        return self.set_error()

