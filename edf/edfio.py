#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob
import pyedflib
import tempfile
import shutil
import csv
import io
from datetime import datetime
from copy import deepcopy
from itertools import zip_longest

import pprint
pp = pprint.PrettyPrinter(indent=4)

try:
    import logging
    from settings import *
    logger = logging.getLogger(__name__)
    logger.addHandler(logging.FileHandler(LOG_FILE))
    logger.setLevel(logging.INFO)
except:
    logger = False

class EDFIO(object):

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, val):
        return setattr(self, key, val)

    # обновить информацию о последней ошибке
    def set_error(self, status=0, text=None):
        self.status = status
        self.error = text
        return status

    def Logg(self,  mess):
        if logger:
            logger.info(
                "[{pid}] {time} {mess}".format(
                    pid=os.getpid(),
                    time=datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S.%f"),
                    mess=mess,
                )
            )


    def __init__(self):
        self.edf = None
        self.files = {}
        self.set_error()

    # получить все файлы, соотвествующие маске
    def get_files_on_mask(self, mask):
        files = glob.glob(mask)
        return files

    # высрать чонить в дебуг
    def debug(self, text):

        debug_file = self.files.get('debug').get('file')
        if not debug_file:
            return self.set_error(103, "debug file not opened")

        dt = '{0:%Y-%m-%d %H:%M:%S} : '.format(datetime.now())

        if type(text) == str:
            text = dt + text + '\n'
            debug_file.write(text)
        else:
            self.pp.pprint(text)
            debug_file.write('\n')

        return self.set_error()

    # прочитать из буфера length байтов, продвинуть позицию
    def read_from_buf(self, buf, length):
        ans = buf[self.pos:self.pos+length]
        self.pos += length
        # костыли на обработку кривых данных balert
        if ans == b'-3276.8 ':
            ans = b'-32768 '
        if ans == b'3276.7  ':
            ans = b'32767  '
        return ans

    # прочитать из буфера заголовок файла БДФ \ ЕДФ
    def read_header(self, buf):
        if buf == None or len(buf) == 0:
            return self.set_error(101, "Empty header")

        params = {}
        try:
            # разбираем заголовок
            f_type = None
            self.pos = 0
            c = self.read_from_buf(buf, 1)
            s = self.read_from_buf(buf, 7).decode('ascii')

            if ord(c) == 255:
                # BDF branch
                f_type = 'BDF'
                if s != 'BIOSEMI':
                    return self.set_error(101, "Incorrect BDF header. BIOSEMI != '{s}'".format(s=s))

            elif ord(c) == 48:
                # EDF branch
                f_type = 'EDF'
                if s != '       ':
                    return self.set_error(101, "Incorrect EDF header. '       ' != '{s}'".format(s=s))

            else:
                return self.set_error(101, "Incorrect BDF / EDF header in file. First char={s}".format(s=ord(c)))

            params['file_type'] = f_type

            params['local_subject_id']   = self.read_from_buf(buf, 80).decode('ascii').strip()
            params['local_recording_id'] = self.read_from_buf(buf, 80).decode('ascii').strip()
            params['date']               = self.read_from_buf(buf, 8).decode('ascii')
            params['time']               = self.read_from_buf(buf, 8).decode('ascii')
            params['header_len']         = self.read_from_buf(buf, 8).decode('ascii').strip()
            params['version_data_format'] = self.read_from_buf(buf, 44).decode('ascii').strip()
            params['record_count']       = self.read_from_buf(buf, 8).decode('ascii').strip()
            params['duration']           = self.read_from_buf(buf, 8).decode('ascii').strip()
            params['signal_count']       = int(self.read_from_buf(buf, 4).decode('ascii').strip())

            # сколько в 1 фрейме всего будет сэмплов
            params['samples'] = 0

            # сколько  байтов на сэмпл
            params['bytes_per_sample'] = 0
            if params['version_data_format'] in ['24BIT', 'BDF+C', 'BDF']:
                params['bytes_per_sample'] = 3
            elif params['version_data_format'] in ['BIOSEMI', 'EDF+C', 'EDF']:
                params['bytes_per_sample'] = 2

            params['canal_data'] = []
            params['canals'] = []

            for i in range(params['signal_count']):
                canal = {}
                canal['label'] = self.read_from_buf(buf, 16).decode('ascii').strip()
                params['canals'].append(canal)

            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['transducer'] = self.read_from_buf(buf, 80).decode('ascii').strip()

            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['dimension'] = self.read_from_buf(buf, 8).decode('ascii').strip()

            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['physical_min'] = int(self.read_from_buf(buf, 8).decode('ascii').strip())

            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['physical_max'] = int(self.read_from_buf(buf, 8).decode('ascii').strip())

            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['digital_min'] = int(self.read_from_buf(buf, 8).decode('ascii').strip())

            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['digital_max'] = int(self.read_from_buf(buf, 8).decode('ascii').strip())

            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['prefilter'] = self.read_from_buf(buf, 80).decode('ascii').strip()

            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['samples'] = int(self.read_from_buf(buf, 8).decode('ascii').strip())

                # В балерте, данные каналы имеет неправильное кол-во сэмплов
                if params['local_recording_id'] == "Neurotrend EEG record" \
                        and (canal['label'] in ['ESUTimestamp', 'SystemTimestamp']):
                    canal['samples'] = params['canals'][0]['samples']
                params['samples'] += canal['samples']

            params['bytes_per_record'] = params['bytes_per_sample'] * params['samples']
            #pp.pprint(params)

        except:
            return self.set_error(101, "Incorrect BDF / EDF header in file. Exception={e}".format(
                e=sys.exc_info(),
            ))

        self.last_read_params = params
        return self.set_error()

    #
    def read_data(self, buf, offset=0):
        if buf == None or len(buf) == 0:
            return self.set_error(101, "Empty header")
        length = len(buf)

        # прочитать из файла данные ЕДФ / БДФ в соответствии с имеющимся заголовком
        header = self.files.get('header', {}).get('params')
        if not header:
            return self.set_error(110, "Cant read data-only file without header")

        # параметры для данного файла берутся из прочитанного хедера
        params = deepcopy(header)

        # сколько байт в одном сэмпле
        bit_format = header.get('version_data_format')
        bytes = header.get('bytes_per_sample')
        if bytes == 0:
            return self.set_error(101, "Incorrect BDF / EDF header, unknown format")

        # сколько описано каналов
        sig_count = header.get('signal_count', 0)
        if sig_count <= 0:
            return self.set_error(101, "Incorrect BDF / EDF header, no signal_count")

        canals = header.get('canals', [])
        if len(canals) != sig_count:
            return self.set_error(101, "Incorrect BDF / EDF header, signals count != signal_count")

        record_size = 0
        canal_data = []
        # считаем сколько весят данные со всех каналов для одного record
        for c_index in range(sig_count):
            canal = canals[c_index]
            canal_data.append([])
            samples = canal['samples']
            record_size += samples * bytes

        # если задано смещение во фреймах - смещаем
        self.pos = offset * record_size

        # проверяем что в файле - целое число фреймов
        iterations = (length - self.pos) / record_size
        #"""
        self.Logg('file_size={fs} record_size={rs} offset={o} iter={it}'.format(
            fs=length,
            rs=record_size,
            o=self.pos,
            it=iterations,
        ))
        #"""
        if iterations != int(iterations):
            return self.set_error(103, "Incorrect file format. file_size != n * record_size")

        try:
            # пока все еще есть непрочитанные данные, читаем
            #params['raw_data'] = b''
            while length - self.pos >= record_size:

                # читаем данные каждого канала по указанному числу сэмплов
                for c_index in range(sig_count):
                    canal = canals[c_index]
                    samples = canal['samples']

                    for i in range(samples):
                        """
                        # decipher from bdf format
                        # функция распаковки 3 байт в double
                        auto unpack24BitValue = [](uchar *buf) -> double {
                            int intValue = ((buf[2] << 16) | (buf[1] << 8) | buf[0]);
                            if (intValue >= 8388608) intValue -= 16777216;
                            return (double)intValue;
                        };
                        # само конвертирование данные из фрэйма BDF в данные по каналам (j - канал, i - индекс значения)
                        frameData.Channels[j].Value[i] = (unpack24BitValue(ptr) * 0.000447) / 10.0 / 1000.0;
                        # далее это еще надо умножить на 1000000
                        """
                        v = int.from_bytes(
                            self.read_from_buf(buf, bytes),
                            byteorder = 'little',
                        )
                        if v >= 8388608:
                            v -= 16777216
                        v = int(v * 0.0447)

                        # remember
                        canal_data[c_index].append(v)

            # все прочитали
            #self.Logg('canal_data={c}'.format(c=canal_data))
            params['canal_data'] = canal_data

            # возможно надо для каждого канала в инфе проставить сколько мы там начитали
            pass

        except:
            return self.set_error(101, "Error reading BDF data from file! Exception={e}".format(
                e=sys.exc_info(),
            ))

        self.last_read_params = params
        return self.set_error()


    # открыть файло с определенными параметрами, запомнить его по названию типа
    def open_file(self, fname, right, typ, offset=0):
        """
        'r'     open for reading (default)
        'w'     open for writing, truncating the file first
        'x'     open for exclusive creation, failing if the file already exists
        'a'     open for writing, appending to the end of the file if it exists
        'b'     binary mode
        't'     text mode (default)
        '+'     open a disk file for updating (reading and writing)
        """

        print('open_file', fname, right, typ)

        if not fname or type(fname) != str:
            return self.set_error(102, "Invalid file name")
        if not right or type(right) != str:
            return self.set_error(102, "Invalid right")

        if not typ or type(typ) != str:
            return self.set_error(102, "Invalid file name")

        if self.files.get(typ):
            return self.set_error(101, typ + " file already opened")
        if typ == 'in' and not os.path.exists(fname):
            return self.set_error(102, "Invalid file name")

        f = edf = None
        params = {}
        if typ == 'in':
            # читаем edf / bdf и делаем сводку в словаре
            try:
                # Создаем временный файл для чтения копируем файл во временный,
                # путь временного файла даем pyedflib.EdfReader, при таком подходе
                # имена с кирилицей читает, иначе выводит ошибку для файлов с кирилицей
                temp = tempfile.NamedTemporaryFile()
                shutil.copyfile(fname, temp.name)
                temp.seek(0)
                edf = pyedflib.EdfReader(temp.name)
                params = {
                    'fname': fname,
                    'signal_count': int(edf.signals_in_file),
                    'duration': float(edf.file_duration),
                    'start_dt': edf.getStartdatetime(),
                    'record_count': edf.datarecords_in_file,
                    'annot_count': edf.annotations_in_file,
                    'canals': [],
                    'canal_data': [],
                }
                for canal in range(edf.signals_in_file):
                    params['canals'].append({
                        'label': edf.getLabel(canal),
                        'dimension': edf.getPhysicalDimension(canal),
                        'samples': int(edf.getNSamples()[canal]),
                        'sample_rate': int(edf.getSampleFrequency(canal)),
                        'physical_max': float(edf.getPhysicalMaximum(canal)),
                        'physical_min': float(edf.getPhysicalMinimum(canal)),
                        'digital_max': int(edf.getDigitalMaximum(canal)),
                        'digital_min': int(edf.getDigitalMinimum(canal)),
                        'prefilter': edf.getPrefilter(canal),
                        'transducer': edf.getTransducer(canal),
                    })

            except Exception as e:
                return self.set_error(100, "Error open for reading edf file {f}. {e}".format(
                    f=fname,
                    e=e,
                ))


        elif typ == 'out':
            try:
                # check we can write file
                # файл мы откроем когда уже зафигачим все каналы, при его открытии надо знать количество каналов
                f = open(fname, 'w')
                f.close()

                params = {
                    'fname': fname,
                    'signal_count': 0,
                    'duration': 0,
                    'start_dt': None,
                    'record_count': 0,
                    'annot_count': 0,
                    'canals': [],
                    'canal_data': [],
                }

            except Exception as e:
                return self.set_error(100, "Error open for wtiting edf file " + str(fname))


        elif typ == 'debug':
            try:
                debug_folder = os.path.dirname(fname)
                if not os.path.exists(debug_folder):
                    print('create ', debug_folder)
                    os.makedirs(debug_folder)

                f = open(fname, right)
                self.pp = pprint.PrettyPrinter(
                    indent=4,
                    stream=f,
                )
            except:
                return self.set_error(100, "Cant open file " + fname)


        elif typ == 'header':
            # читаем файло с заголовком
            try:
                f = open(fname, 'rb')
                buf = f.read()
                f.close()
                f = None
            except:
                return self.set_error(100, "Cant open file " + fname)

            ans = self.read_header(buf)
            if ans != 0:
                return ans

            params = self.last_read_params

        elif typ == 'data':
            #self.Logg('EDFIO open file type=data')

            # читаем файло
            file_size = 0
            try:
                # узнаем длину файла
                #file_size = os.path.getsize(fname)
                f = open(fname, 'rb')
                buf = f.read()
                f.close()
                f = None
            except:
                return self.set_error(100, "Cant open file " + fname)

            ans = self.read_data(buf, offset)
            if ans != 0:
                return ans

            params = self.last_read_params

        else:
            try:
                f = open(fname, right)
            except:
                return self.set_error(100, "Cant open file " + fname)

        self.files[typ] = {
            'file': f,
            'edf': edf,
            'params': params,
        }
        return self.set_error()

    #
    def out_add_channel(self, summary, data):
        typ = 'out'
        if typ not in self.files:
            return self.set_error(110, "Out file is not set, cant save")

        p = self.files[typ].get('params')
        if not p:
            return self.set_error(111, "Out file params are not set")

        p['canals'].append(summary)
        p['canal_data'].append(data)
        p['signal_count'] += 1
        return self.set_error()

    #
    def out_save(self):
        # сохраняет данные в out-file
        typ = 'out'
        if typ not in self.files:
            return self.set_error(110, "Out file is not set, cant save")

        p = self.files[typ].get('params')
        if not p:
            return self.set_error(111, "Out file params are not set")

        signal_count = p.get('signal_count')
        if not signal_count or signal_count < 1:
            return self.set_error(112, "Incorrect signal count parameter")

        duration = p.get('duration')
        if not duration or duration < 1:
            return self.set_error(113, "Incorrect duration parameter: " + str(duration))

        canal_info = p.get('canals')
        if not canal_info or type(canal_info) != list or len(canal_info) != signal_count:
            return self.set_error(114, "Incorrect canals summary!")

        canal_data = p.get('canal_data')
        if not canal_data or type(canal_data) != list or len(canal_data) != signal_count:
            return self.set_error(115, "Incorrect canals data!")

        fname = p.get('fname')
        if not fname:
            return self.set_error(116, "Incorrect fname parameter!")

        try:
            # Создаем временный файл который не удаляется при закрытии
            # закрываем файл затем копируем временный файл
            # удаляем временный файл
            # без использования временного файла pyedflib.EdfWriter кирилица не работает
            fd, path = tempfile.mkstemp()
            f = pyedflib.EdfWriter(
                path,
                signal_count,
                file_type=pyedflib.FILETYPE_BDFPLUS         # edflib.FILETYPE_EDFPLUS
            )
            f.setSignalHeaders(canal_info)
            f.writeSamples(canal_data)
            f.setDatarecordDuration(duration)
            f.close()
            shutil.copyfile(path, fname)
            os.remove(path)
        except Exception as e:
            raise(e)
            return self.set_error(117, "Error writing edf file " + str(dict(e)))

        return self.set_error()

    # закрыть нах файл по его типу
    def close_file(self, typ):
        if not typ or type(typ) != str:
            return self.set_error(102, "Invalid file name")
        if not self.files.get(typ):
            return self.set_error(103, typ + " file not opened")

        try:
            if self.files[typ]['file']:
                self.files[typ]['file'].close()
            if self.files[typ]['edf']:
                self.files[typ]['edf']._close()
        except:
            #return self.set_error(100, "Error closing file " + fname)
            pass

        del self.files[typ]
        return self.set_error(0)


    # получить номера каналов по названиям
    def get_channels_numbers_by_names(self, typ='header', eeg_channels=[]):
        eeg_channel_numbers = []
        header = self.files.get(typ, {}).get('params')
        if header == None:
            return []

        for c in eeg_channels:
            c_count = len(header['canals'])
            for i in range(c_count):
                canal = header['canals'][i]
                if canal['label'] == c:
                    eeg_channel_numbers.append(i)

        return eeg_channel_numbers

    # новая функция обработки bdf/edf данных
    def read_data_new(self, buf, offset=0):
        if buf == None or len(buf) == 0:
            return self.set_error(101, "Empty header")
        length = len(buf)

        # прочитать из файла данные ЕДФ / БДФ в соответствии с имеющимся заголовком
        header = self.files.get('header', {}).get('params')
        if not header:
            return self.set_error(110, "Cant read data-only file without header")

        # параметры для данного файла берутся из прочитанного хедера
        params = deepcopy(header)

        # сколько байт в одном сэмпле
        bit_format = header.get('version_data_format')
        bytes = header.get('bytes_per_sample')
        if bytes == 0:
            return self.set_error(101, "Incorrect BDF / EDF header, unknown format")

        # сколько описано каналов
        sig_count = header.get('signal_count', 0)
        if sig_count <= 0:
            return self.set_error(101, "Incorrect BDF / EDF header, no signal_count")

        canals = header.get('canals', [])
        if len(canals) != sig_count:
            return self.set_error(101, "Incorrect BDF / EDF header, signals count != signal_count")

        record_size = 0
        canal_data = []

        format_file = header.get('local_recording_id')
        # определяем нестандартный формат balert
        if format_file == 'Neurotrend EEG record':
            balert = True
        else:
            balert = False

        # считаем сколько весят данные со всех каналов для одного record
        for c_index in range(sig_count):
            canal = canals[c_index]
            canal_data.append([])
            samples = canal['samples']
            # если формат файла балерт, то на разные каналы, разные данные, иначе стандарт
            if balert:
                if c_index == 24:
                    canal_bytes = 4
                elif c_index == 25:
                    canal_bytes = 8
                else:
                    canal_bytes = 2
            else:
                canal_bytes = bytes
            record_size += samples * canal_bytes

        # если задано смещение во фреймах - смещаем
        self.pos = offset * record_size

        # проверяем что в файле - целое число фреймов
        iterations = (length - self.pos) / record_size
        #"""
        self.Logg('file_size={fs} record_size={rs} offset={o} iter={it}'.format(
            fs=length,
            rs=record_size,
            o=self.pos,
            it=iterations,
        ))
        #"""
        if iterations != int(iterations):
            return self.set_error(103, "Incorrect file format. file_size != n * record_size")

        try:
            # пока все еще есть непрочитанные данные, читаем
            #params['raw_data'] = b''

            while length - self.pos >= record_size:

                # читаем данные каждого канала по указанному числу сэмплов
                for c_index in range(sig_count):
                    canal = canals[c_index]
                    samples = canal['samples']

                    # для каждого канала определяем gain
                    phys = canal['physical_max'] - canal['physical_min']
                    dig = canal['digital_max'] - canal['digital_min']
                    if dig == 0.0:
                        dig = 1.0
                    gain = phys / dig
                    for i in range(samples):
                        """
                        # decipher from bdf format
                        # функция распаковки 3 байт в double
                        auto unpack24BitValue = [](uchar *buf) -> double {
                            int intValue = ((buf[2] << 16) | (buf[1] << 8) | buf[0]);
                            if (intValue >= 8388608) intValue -= 16777216;
                            return (double)intValue;
                        };
                        # само конвертирование данные из фрэйма BDF в данные по каналам (j - канал, i - индекс значения)
                        frameData.Channels[j].Value[i] = (unpack24BitValue(ptr) * 0.000447) / 10.0 / 1000.0;
                        # далее это еще надо умножить на 1000000
                        """
                        # если формат файла балерт, то на разные каналы, разные данные, иначе стандарт
                        if balert:
                            if c_index == 24:
                                canal_bytes = 4
                            elif c_index == 25:
                                canal_bytes = 8
                            else:
                                canal_bytes = 2
                            signed = True
                        else:
                            canal_bytes = bytes
                            signed = False
                        v = int.from_bytes(
                            self.read_from_buf(buf, canal_bytes),
                            byteorder='little',
                            signed=signed
                        )

                        # Разная обработка данных для balert, edf, bdf
                        if bytes == 3:
                            if v >= 8388608:
                                v -= 16777216
                            v = int(v)
                            v = (v * gain)
                            if c_index < 8:
                                v = v / 1000.0
                        elif balert:
                            if c_index < 24:
                                v = v / 10.0
                        else:
                            v = v * gain

                        # remember
                        canal_data[c_index].append(v)

            # все прочитали
            #self.Logg('canal_data={c}'.format(c=canal_data))
            params['canal_data'] = canal_data

            # возможно надо для каждого канала в инфе проставить сколько мы там начитали
            pass

        except:
            return self.set_error(101, "Error reading BDF data from file! Exception={e}".format(
                e=sys.exc_info(),
            ))

        self.last_read_params = params
        return self.set_error()


    # Сохранить или вернуть EDF/BDF
    def save_bdf_to_csv(self, fin_path, fout_path=False, channel_count=24):
        # открываем файл
        ans = self.open_file(fin_path, 'r', 'header')
        if ans != 0:
            return ans

        # загоняем файл в буфер и отрезаем заголовок
        with open(fin_path, 'rb') as f:
            buf = f.read()
            ans = self.read_data_new(buf[int(self.last_read_params['header_len']):])
        if ans != 0:
            return ans

        # собираем полученные данные
        channel_name_list = []
        channel_data = []
        for i in range(int(self.last_read_params['signal_count'])):
            if i + 1 > channel_count:
                break
            channel_name_list.append(self.last_read_params['canals'][i]['label'])
            channel_data.append(self.last_read_params['canal_data'][i])
        export_data = zip_longest(*channel_data, fillvalue='')

        # создаем csv файл или помещаем в переменную
        try:
            if fout_path:
                with open(fout_path, 'w', newline='') as csvfile:
                    wr = csv.writer(csvfile, delimiter='\t')
                    wr.writerow(channel_name_list)
                    for data in export_data:
                        wr.writerow(data)
                return self.set_error()
            else:
                buffer_data = io.StringIO()
                wr = csv.writer(buffer_data, delimiter='\t')
                wr.writerow(channel_name_list)
                for data in export_data:
                    wr.writerow(data)
                self.csv = buffer_data.getvalue()
                return self.set_error()
        except:
            return self.set_error(103, "Error in create csv! Exception={e}".format(
                e=sys.exc_info(),
            ))

    def cut_bdf(self, start_time, end_time, channel_number_list, out_format='csv'):
        if len(channel_number_list) == 0:
            return self.set_error(120, "Channel list empty")

        data_list = []
        channel_name_list = []
        # для каждого переданного канала обрезаем данные
        for c in channel_number_list:
            if c > len(self.last_read_params['canals']):
                return self.set_error(121, "Channel - {} not found".format(c))

            channel_name_list.append(self.last_read_params['canals'][c]['label'])

            # Находим кол-во записей в секунду для каждого канала
            record_sec = (1 / float(self.last_read_params['duration'])) * self.last_read_params['canals'][c]['samples']
            record_start = int(record_sec * start_time)
            record_end = int(record_sec * end_time)

            data = self.last_read_params['canal_data'][c][record_start:record_end]
            data_list.append(data)

        # Для разного формата - разная выдача данных
        if out_format == 'csv':
            export_data = zip_longest(*data_list, fillvalue='')
            buffer_data = io.StringIO()
            wr = csv.writer(buffer_data, delimiter='\t')
            wr.writerow(channel_name_list)
            for data in export_data:
                wr.writerow(data)
            self.csv = buffer_data.getvalue()
            return self.set_error()

        else:
            return self.set_error(122, "Unknow format = {}".format(
                out_format,
            ))






