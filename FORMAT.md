Поднимаем простой веб-сервер, который открывает доступ к папкам на винте. В папках лежат файлы.

Url вида: host/experiments/<ExperimentName>.json

{
    "CanalCount": 3,
    "Canals": [{
        "CanalUrl": "CanalUrl",         # пока не поддерживается, да и не фактт что надо
        "CanalName": "ADS_ch1"
    }, {
        "CanalUrl": "CanalUrl",
        "CanalName": "EEG_RX_N"
    }, {
        "CanalUrl": "CanalUrl",
        "CanalName": "BDF Annotations"
    }],
    "ExperimentName": "2"
}



Url для канала такого вида: host/experiments/<ExperimentName>/<CanalName>.json

{
    "label": "ACC_X",
    "ph_min": -2000.0,
    "ph_dimension": "mg",
    "dig_max": 8191.0,
    "prefilter": "No filtering",
    "Data": [
        -192.27247756821095,
        -190.31923335164498,
        -192.0283220411402,
        -193.98156625770616,
        -188.36598913507905
    ],
    "frequency": 50,
    "dig_min": -8192.0,
    "GraphicType": 0,
    "transducer": "",
    "ph_max": 2000.0,
    "CanalName": "ACC_X",
    "samples": 4110
}