from distutils.core import setup

setup(name='edf',
      version='1.1',
      description="EDF Utilities",
      author='NeuroTrend',
      author_email='info@neurotrend.ru',
      url='https://gitlab.com/sys_neurotrend/bdf',
      license='MIT',
      platforms=['all'],
      packages=['edf'],
)
