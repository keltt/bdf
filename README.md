# prepare virtual env
virtualenv -p python3 .env

# activate venv
. .env/bin/activate

# install dependencies
pip install -r requirements.txt

# собрать пакет EDF
не включая env после любых правок в файлах EDF
пересобрать пакет скриптом:

./make_dist.sh

отправить изменения в репозиторий (edf-1.0.tar.gz)

# В проектах в requirements.txt
Пересобрать контейнер проекта ./docker__up
с изменениями в requirements.txt приписав в самый конец Git
#
...

pyEDFlib==0.1.14

pytz==2018.9

...

git+https://gitlab.com/sys_neurotrend/bdf.git#bdf

#

# В проектах в коде
импорты пакета from edf.edf import * или from edf.edf import EDF


