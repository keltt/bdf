#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
import codecs
import numpy as np
from edf.edf import EDF

CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
KEY_LEN = 4

def key_to_int(key=""):
    power = 0
    ans = 0
    max_digit = len(CHARS)
    for c in key:
        if power >= KEY_LEN:
            break
        num = CHARS.find(c)
        ans += num * (max_digit ** power)
        power += 1
    return ans

def int_to_key(n=None):
    max_digit = len(CHARS)
    ans = ""
    while len(ans)< KEY_LEN:
        ans += CHARS[n % max_digit]
        n = n // max_digit
    return ans


def add(key="", add=1):
    max_digit = len(CHARS)

    # строковый ключ в массив
    lst = []
    for c in key:
        lst.append(CHARS.find(c))

    # прибавляем число в СС с основанием, равным длине массива возможных символов
    lst2 = []
    for e in reversed(lst):
        lst2.append((e + add) % max_digit)
        add = (e + add) // max_digit
    if add > 0:
        lst2.append(add)

    # делаем обратно строку
    ans = ""
    for e in reversed(lst2):
        ans = ans + CHARS[e]
    return ans

def max_key():
    n = KEY_LEN
    max_digit = len(CHARS)
    ans = 0
    while n > 0:
        ans += max_digit ** n
        n -= 1
    return ans


key = 'ZZZZ'
print(key, '\n')

lis = add(key)
print(lis)

"""
num = key_to_int(key)
print(num, '\n')
key = int_to_key(num)
print(key, '\n')

print(max_key(), '\n')
"""
exit()

#fname = "samples/2.bdf"
#dout = "/home/martynov/graphics/out/"

fname = "/home/martynov/graphics/samples/2.bdf"
dout = "/home/martynov/graphics/multifileload/data"
fout = "/home/martynov/graphics/22.bdf"

e = EDF({'debug': 'debug.log'})
res = e.open_file(fname, 'r', 'in')
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Open in - OK!")

res = e.open_file(fout, 'w', 'out')
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Open out - OK!")

edf = e.files['in']
params = edf['params']
sc = params['signal_count']
duration = params['duration']
e.files['out']['params']['duration'] = duration

for c in range(sc):
    # готовим данные по каждому каналу
    summary = params['canals'][c]
    e.debug(summary)

    res = e.read_signal(c)
    if res != 0:
        e.debug(e.error)
        exit()
    data = np.asarray(e.result)

    # перефигачиваем их в выходной файл
    res = e.out_add_channel(summary, data)
    if res != 0:
        e.debug(e.error)
        exit()
    e.debug("Add channel - OK!")

# add 1 more channel
summary = {
    'label': 'squarewave',
    'dimension': 'uV',
    'sample_rate': 200,
    'physical_max': 100,
    'physical_min': -100,
    'digital_max': 32767,
    'digital_min': -32768,
    'transducer': '',
    'prefilter':''
}

time = np.linspace(0, duration, duration*200)
xtemp = np.sin(2*np.pi*0.1*time)
x1 = xtemp.copy()
x1[np.where(xtemp > 0)[0]] = 100
x1[np.where(xtemp < 0)[0]] = -100

res = e.out_add_channel(summary, x1)
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Add channel - OK!")

# сохраняем выходной файл
res = e.out_save()
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Save - OK!")


